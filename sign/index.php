<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Your Email Signature</title>
	<link rel="stylesheet" type="text/css" href="../css/style.css" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script type="text/javascript" src="../js/main.js"></script>
</head>

<body>
	<div id="sig">
		<div id="sigToCopy">
		    <img src="http://stagingenv.com.au/wattsdesign-signature/email-img/WattsLogo_New.svg" height="35" width="150"  style="height: 35px;margin-bottom: 20px; margin-top: 15px;" />
            
            <p style="color:#000; font-family:brandon-grotesque, Verdana, Geneva, Tahoma, sans-serif; font-size:15px;margin:0;padding:0; font-weight: bold; text-transform: uppercase; padding-bottom: 0;"><?php echo $_POST['name'] ?><span style="font-size: 15px; font-weight: normal;"> | </span><span style="font-family: brandon-grotesque, Verdana, Arial, sans-serif; font-size: 15px; font-weight: normal;"><?php echo $_POST['position'] ?></span></p>
            
            <p style="color:#000; font-family:brandon-grotesque, Verdana, Geneva, Tahoma, sans-serif; font-size:15px;margin:0;padding:0; font-weight: normal; text-transform: uppercase; padding-bottom: 0;"><?php echo $_POST['address'] ?></p>
            
            <p style="margin-bottom: 3px; font-size: 15px;"><a style="font-family: brandon-grotesque, Verdana, Geneva, Tahoma, sans-serif; color:#000; text-decoration: none; margin-bottom: 10px;" href="tel:<?php echo $_POST['phone'] ?>"><span style="font-family: brandon-grotesque, Verdana, Geneva, Tahoma, sans-serif; font-weight: bold">P</span> <?php echo $_POST['phone']?></a> | <a style="font-family: brandon-grotesque, Verdana, Geneva, Tahoma, sans-serif; color:#000; text-decoration: none; margin-bottom: 10px;" href="tel:<?php echo $_POST['mobile'] ?>"><span style="font-family: brandon-grotesque, Verdana, Geneva, Tahoma, sans-serif; font-weight: bold">M</span> <?php echo $_POST['mobile']?></a></p>
            
            <p style=" font-family: brandon-grotesque, Verdana, Geneva, Tahoma, sans-serif; margin-bottom: 3px; font-size: 15px;"><a style="color:#000; text-decoration: none; margin-bottom: 10px; text-transform: uppercase;" href="mailto:<?php echo $_POST['email']?>"><?php echo $_POST['email']?></a></p>
            
            <p style="font-family: brandon-grotesque, Verdana, Geneva, Tahoma, sans-serif; margin-bottom: 20px; font-size: 15px;"><a style="color:#000; text-decoration: none; margin-bottom: 10px; text-transform: uppercase; font-weight: bold" target="_blank" href="https://wattsdesign.com.au">wattsdesign.com.au</a></p>
            
            <p style="color:#a39f9f; font-family:brandon-grotesque, Verdana, Geneva, Tahoma, sans-serif; font-size:13px;margin:0;padding:0; font-weight: normal; padding-bottom: 5px;">The contents of this email and its attachments may not be necessarily be those of Watts Design. This email is intended for the names recepients only and only attachments that it may contain is
            privileged and confidentail information. If you have received this email in error please notify the sender and delete it immediately. And confidentially, privilege or copyright is not waived
            or lost if you have received this email in error. This email and its attachments have been scanned for errors, however it is your responsibility to check this email for any attachments. Thanks</p>
		</div>
	</div>
	
	<div class="copy">
	<button onclick="CopyToClipboard('sigToCopy')">Copy signature to clipboard</button>
	</div>
</body>
</html>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
	<link rel="stylesheet" type="text/css" href="css/style.css" />
</head>

<body>
	

	<div id="sig">
	
		<h1>Complete the form below to customise your email signature</h1>
	
	<form action="sign/" method="post" enctype="multipart/form-data" >
	
	    <p><label for="name">Your first and last name</label><br>
		<input type="text" name="name" value="" required /></p>

	    <p><label for="position">Your position</label><br>
		<input type="text" name="position" value=""/></p>
		
		<p><label for="address">Address</label><br>
		<input type="text" name="address" value=""/></p>
		
		<p><label for="phone">Phone</label><br>
		<input type="text" name="phone" value=""/></p>

		<p><label for="mobile-phone">Your mobile phone number</label><br>
		<input type="text" name="mobile" value="" required /></p>

		<p><label for="email">Your email address</label><br>
		<input type="text" name="email" value="" required /></p>
		
		<input type="submit" value="Create Signature"/>

	</form>

	</div>
	
</body>
</html>